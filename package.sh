#!/bin/bash
set -e
set -x

rm divi-previous-next-buttons.zip
zip -r divi-previous-next-buttons.zip divi-previous-next-buttons -x *node_modules*
