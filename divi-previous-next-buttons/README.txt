=== Divi Previous and Next Buttons ===

Want to easily add previous and next buttons to posts and custom post types? Look no further.

Pull requests are welcome at https://gitlab.com/benevolenttech/divi-previous-next-buttons

== Quick Overview ==

* Installation
* Rollback/Uninstall
* How it Works
* Where it Works
* What it Changes
* Usage
* Limitations
* Support
* Credits
* License

== Installation ==

Install using the standard Wordpress "Add Plugin" feature. 

Manual Installation: download, unpack and move this folder into your 'plugins' folder. Then activate it using `wp plugin activate divi-previous-next-button` or using the Wordpress admin area.

For developers: clone the [github repo](https://gitlab.com/benevolenttech/divi-previous-next-buttons) and follow the instructions in [Create Divi Extension](https://github.com/elegantthemes/create-divi-extension) to develop.

== Rollback ==

Thus plugin doesn't write anything to the database, so feel free to remove, downgrade or upgrade with minimal risk.

== How It Works ==

Overrides the standard button modules to dynamically point to the previous or next published post of the same type. Works with pages and custom posts, too!

== Where It Works  ==

The plugin kicks in when a user selects a Divi module to add to a post, and when editing or viewing a post using one of the provided modules.

== What It Changes ==

The target link of the button.

== Usage ==

When adding a Divi module to a post, select either "Previous Button" or "Next Button" for a module. Then customize the style as needed and your done!

== Limitations ==

* The modules do not preview correctly in the visual builder. I've therefore disabled the preview feature. Would love a recommendation or pull request on how to fix that!
* Icon position and size don't work properly. No idea why.

== Support ==

If you have any issues with this plugin feel free to reach out to the maintainer, bdombro@gmail.com.

== Credits ==

* This project was inspired by this tutorial --> https://www.elegantthemes.com/documentation/developers/divi-module/how-to-create-a-divi-builder-module/
* This project was bootstrapped with [Create Divi Extension](https://github.com/elegantthemes/create-divi-extension).

== License ==

GNU GPL v2 or later

== Changelog ==

= 1.0.1 =
Enabled more button customizer features

= 1.0.0 =
Initial release
