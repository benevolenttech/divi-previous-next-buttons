// External Dependencies
import React, { Component } from 'react';

// Internal Dependencies
import './style.css';


class PreviousButton extends Component {

  static slug = 'dpnb_previous_button';

  render() {

    return (
        <this.props.content button_url="https://google2.com" />
    );
  }
}

export default PreviousButton;
