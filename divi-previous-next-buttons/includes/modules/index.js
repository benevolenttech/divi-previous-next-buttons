import HelloWorld from './HelloWorld/HelloWorld';
import PreviousButton from './PreviousButton/PreviousButton';
import NextButton from './NextButton/NextButton';

export default [HelloWorld,PreviousButton,NextButton];
