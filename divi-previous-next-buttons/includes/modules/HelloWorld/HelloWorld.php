<?php

class DPNB_HelloWorld extends ET_Builder_Module {

	public $slug       = 'dpnb_hello_world';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => 'https://benevolent.tech',
		'author'     => 'Brian Dombrowski',
		'author_uri' => 'https://benevolent.tech',
	);

	public function init() {
		$this->name = esc_html__( 'Hello World', 'dpnb-divi-previous-next-buttons' );
	}

	public function get_fields() {
		return array(
			'content' => array(
				'label'           => esc_html__( 'Content', 'dpnb-divi-previous-next-buttons' ),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Content entered here will appear inside the module.', 'dpnb-divi-previous-next-buttons' ),
				'toggle_slug'     => 'main_content',
			),
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		return sprintf( '<h1>%1$s</h1>', esc_html($this->props['content']) );
	}
}

new DPNB_HelloWorld;
