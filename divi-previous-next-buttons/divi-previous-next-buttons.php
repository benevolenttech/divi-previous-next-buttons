<?php
/*
Plugin Name: Divi Previous Next Buttons
Plugin URI:  https://gitlab.com/benevolenttech/divi-previous-next-buttons
Description: Divi modules for previous and next buttons on posts
Version:     1.0.1
Author:      Brian Dombrowski
Author URI:  https://benevolent.tech
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: dpnb-divi-previous-next-buttons
Domain Path: /languages

Divi Previous Next Buttons is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Divi Previous Next Buttons is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Divi Previous Next Buttons. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'dpnb_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function dpnb_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/DiviPreviousNextButtons.php';
}
add_action( 'divi_extensions_init', 'dpnb_initialize_extension' );
endif;
